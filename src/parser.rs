// Here is where the various combinators are imported. You can find all the combinators here:
// https://docs.rs/nom/5.0.1/nom/
// If you want to use it in your parser, you need to import it here. I've already imported a couple.

use nom::{
  IResult,
  branch::alt,
  //combinator::opt,
  multi::{many1, many0},
  bytes::complete::{tag},
  character::complete::{alphanumeric1, digit1},
};

// Here are the different node types. You will use these to make your parser and your grammar.
// You may add other nodes as you see fit, but these are expected by the runtime.

#[derive(Debug, Clone)]
pub enum Node {
  Program { children: Vec<Node> },
  Statement { children: Vec<Node> },
  FunctionReturn { children: Vec<Node> },
  FunctionDefine { children: Vec<Node> },
  FunctionArguments { children: Vec<Node> },
  FunctionStatements { children: Vec<Node> },
  Expression { children: Vec<Node> },
  MathExpression {name: String, children: Vec<Node> },
  FunctionCall { name: String, children: Vec<Node> },
  VariableDefine { children: Vec<Node> },
  Number { value: i32 },
  Bool { value: bool },
  Identifier { value: String },
  String { value: String },
}

// Define production rules for an identifier
pub fn identifier(input: &str) -> IResult<&str, Node> {
  let (input, result) = alphanumeric1(input)?;                  // Consume at least 1 alphanumeric character. The ? automatically unwraps the result if it's okay and bails if it is an error.
  Ok((input, Node::Identifier{ value: result.to_string()}))     // Return the now partially consumed input, as well as a node with the string on it.
}

// Define an integer number
pub fn number(input: &str) -> IResult<&str, Node> {
  let (input, result) = digit1(input)?;                         // Consume at least 1 digit 0-9
  let number = result.parse::<i32>().unwrap();                  // Parse the string result into a usize
  Ok((input, Node::Number{ value: number}))                     // Return the now partially consumed input with a number as well
}

pub fn boolean(input: &str) -> IResult<&str, Node> {
  let (input, result) = alt((tag("true"),tag("false")))(input)?;  // Parse either true or false
  let bool_result = if result == "true" {true} else {false};      
  Ok((input, Node::Bool{ value: bool_result}))
}

pub fn string(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("\"")(input)?;                                   // Parse the surrounding quotes
  let (input, result) = many0(alt((alphanumeric1, tag(" "))))(input)?;  // Parse spaces and alphanumeric chars
  let mut resulting_string = "".to_string();                            // Declare an empty result string
  for curr_char in result {
    resulting_string += curr_char;                                      // Parse the tagged result into the resulting string
  }
  let (input, _) = tag("\"")(input)?;
  Ok((input, Node::String{ value: resulting_string.to_string()}))
}

pub fn function_call(input: &str) -> IResult<&str, Node> {
  let (input, func_name) = alphanumeric1(input)?;                        // Identify the function name
  let (input, _) = tag("(")(input)?;                                     // Identify parens and corresponding arguments
  let (input, result) = many0(arguments)(input)?;
  let (input, _) = tag(")")(input)?;
  Ok((input, Node::FunctionCall{name: func_name.to_string(), children: result}))
}

pub fn parenthetical_expression(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("(")(input)?;
  let (input, result) = math_expression(input)?;
  let (input, _) = tag(")")(input)?;
  Ok((input, Node::Expression{children: vec![result]}))
}

// In case another cycle of parsing is required
pub fn l4(input: &str) -> IResult<&str, Node> {
  alt((function_call, number, identifier, parenthetical_expression))(input)
}

// Copy of l3 infix
pub fn l3_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, op) = tag("^")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l4(input)?;
  Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))
}

// Copy of l1
pub fn l3(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l4(input)?;
  let (input, tail) = many0(l3_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

// Copy of l1 infix
pub fn l2_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, op) = alt((tag("*"),tag("/")))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l3(input)?;
  Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))

}

// Copy of l1
pub fn l2(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l3(input)?;
  let (input, tail) = many0(l2_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

// L1 - L4 handle order of operations for math expressions 
pub fn l1_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, op) = alt((tag("+"),tag("-")))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l2(input)?;
  Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))

}

pub fn l1(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l2(input)?;
  let (input, tail) = many0(l1_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

pub fn math_expression(input: &str) -> IResult<&str, Node> {
  l1(input)
}

pub fn expression(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(alt((tag(" "), tag("\n"), tag("\t"))))(input)?;                                      // Get rid of whitespace
  let (input, result) = alt((function_call, boolean, math_expression, number, identifier, string))(input)?;   // Check grammar for priority rules
  Ok((input, Node::Expression{children: vec![result]}))
}

pub fn statement(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(alt((tag(" "), tag("\n"), tag("\t"))))(input)?;      // Get rid of whitespace
  let (input, result) = alt((variable_define, function_return))(input)?;      // Check Grammar for priority rules
  let (input, _) = tag(";")(input)?;                                          // Statements end with a ;
  Ok((input, Node::Statement{children: vec![result]}))
}

pub fn function_return(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("return ")(input)?;                       // Parse the string "return"
  let (input, result) = expression(input)?;                      // Check grammar for priority rules
  Ok((input, Node::FunctionReturn{children: vec![result]}))
}

// Define a statement of the form
// let x = expression
pub fn variable_define(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("let ")(input)?;
  let (input, variable) = identifier(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = tag("=")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, expression) = expression(input)?;
  Ok((input, Node::VariableDefine{ children: vec![variable, expression]}))   
}

pub fn arguments(input: &str) -> IResult<&str, Node> {
  let (input, result) = expression(input)?;             // First expression is def an arg
  let (input, mut other) = many0(other_arg)(input)?;    // Check for other args
  let mut args = vec![result];                          // Compose the args
  args.append(&mut other);
  Ok((input, Node::FunctionArguments{ children: args}))
}

pub fn other_arg(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag(",")(input)?;                                      // Parse the comma and whitespace before the arg
  let (input, _) = many0(alt((tag(" "), tag("\n"), tag("\t"))))(input)?;
  let (input, result) = expression(input)?;                               // Parse the arg itself
  Ok((input, result))
}

pub fn function_definition(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(alt((tag(" "), tag("\n"), tag("\t"))))(input)?;    // Get rid of whitespace
  let (input, _) = tag("fn ")(input)?;                                      // Parse according to the defined EBNF format. Function, fn_name, parens, args, parens, brackets, statements, brackets.
  let (input, id) = identifier(input)?;
  let (input, _) = tag("(")(input)?;
  let (input, mut args) = many0(arguments)(input)?;
  let (input, _) = tag(")")(input)?;
  let (input, _) = many0(alt((tag(" "), tag("\n"), tag("\t"))))(input)?;    // Get rid of whitespace
  let (input, _) = tag("{")(input)?;
  let (input, _) = many0(alt((tag(" "), tag("\n"), tag("\t"))))(input)?;    // Get rid of whitespace
  let (input, mut lines) = many1(statement)(input)?;
  let (input, _) = many0(alt((tag(" "), tag("\n"), tag("\t"))))(input)?;    // Get rid of whitespace
  let (input, _) = tag("}")(input)?;

  let mut return_func = vec![id];
  return_func.append(&mut args);
  return_func.append(&mut lines);
  Ok((input, Node::FunctionDefine{children: return_func}))
}

pub fn comment(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("//")(input)?;                                 // Parse comment begin
  let (input, _) = many0(alt((alphanumeric1, tag(" "))))(input)?;     // Parse comment content
  let (input, _) = tag("\n")(input)?;                                 // Consume newline
  Ok((input, Node::Identifier{value: "".to_string()}))
}

pub fn comment_multi_line(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("/*")(input)?;                                               // Parse comment begin
  let (input, _) = many0(alt((alphanumeric1, tag(" "), tag("\n"))))(input)?;        // Parse comment content, including whitespace
  let (input, _) = tag("*/")(input)?;                                               // Parse comment end
  Ok((input, Node::Identifier{value: "".to_string()}))
}

pub fn program(input: &str) -> IResult<&str, Node> {
  let (input, mut result) = many1(alt((function_definition, statement, expression, comment, comment_multi_line)))(input)?;  // See EBNF grammar file for structure of parsing
  Ok((input, Node::Program{ children: result}))       
}
